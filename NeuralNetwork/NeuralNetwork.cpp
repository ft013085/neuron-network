#include <iostream>
#include <vector>
#include <cmath>
#include <functional>
#include <unordered_map>

// Sigmoid function
double sigmoid(double x) {
    return 1 / (1 + exp(-x));
}

// ReLU function
double relu(double x) {
    return x > 0 ? x : 0;
}

// Leaky ReLU function
double leaky_relu(double x) {
    return x > 0 ? x : 0.01 * x;
}

// Tanh function
double tan_h(double x) {
    return std::tanh(x);
}

// Softmax function
double softmax(double x) {
    return exp(x);
}

// A struct to represent a neuron
struct Neuron {
    std::vector<double> weights;
    double bias;
    std::function<double(double)> activation_function;

    static std::unordered_map<double(*)(double), std::string> function_names;

    // Constructor
    Neuron(int num_inputs, double bias_val) {
        weights.resize(num_inputs);
        bias = bias_val;
        activation_function = sigmoid;
    }

    // A method to compute the output of the neuron
    double compute(const std::vector<double>& inputs) {
        double sum = bias;
        for (int i = 0; i < weights.size(); i++) {
            sum += weights[i] * inputs[i];
        }
        return activation_function(sum);
    }

    // A method to update the weights of the neuron
    void update_weights(const std::vector<double>& new_weights) {
        if (new_weights.size() == weights.size()) {
            weights = new_weights;
        }
        else {
            std::cout << "Error: Number of weights does not match" << std::endl;
        }
    }

    // A method to update the bias of the neuron
    void update_bias(double new_bias) {
        bias = new_bias;
    }

    // A method to update the activation function of the neuron
    void update_activation_function(std::function<double(double)> new_activation_function) {
        activation_function = new_activation_function;
    }

    // Get the name of the current activation function
    std::string get_activation_function_name() {
        auto it = function_names.find(*activation_function.target<double(*)(double)>());
        if (it != function_names.end()) {
            return it->second;
        }
        return "Unknown";
    }
};

// Define the static member variable
std::unordered_map<double(*)(double), std::string> Neuron::function_names = {
    { sigmoid, "sigmoid" },
    { relu, "relu" },
    { leaky_relu, "leaky_relu" },
    { tan_h, "tanh" },
    { softmax, "softmax" }
};

int main() {
    Neuron neuron(2, 0.0);
    // Set the weights of the neuron
    neuron.update_weights({ 0.5, 0.5 });
    // Set the bias of the neuron
    neuron.update_bias(0.0);
    // Set the activation function of the neuron
    neuron.update_activation_function(softmax);

    // Print the type of activation function
    std::cout << "Activation function: " << neuron.get_activation_function_name() << std::endl;

    // Compute the output of the neuron
    std::vector<double> inputs = { 1.0, 1.0 };

    double output = neuron.compute(inputs);
    std::cout << "Output: " << output << std::endl;
    return 0;
}
